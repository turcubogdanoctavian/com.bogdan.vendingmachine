package com.bogdan.vendingmachine.item.model;

public abstract class Item {

	public Item() {

	}

	public abstract void setName(String name);

	public abstract String getName();

	public abstract void setType(String name);

	public abstract String getType();

	public abstract void setPrice(Double price);

	public abstract double getPrice();

	public abstract void setId(Integer id);

	public abstract Integer getId();

	public abstract void setQuantity(Integer qty);

	public abstract Integer getQuanity();

	@Override
	public String toString() {
		return " | ID: " + this.getId() + " -- " + this.getName() + "-" + this.getType() + " -- priced:"
				+ this.getPrice() + " -- Qty:" + this.getQuanity();
	}

}
