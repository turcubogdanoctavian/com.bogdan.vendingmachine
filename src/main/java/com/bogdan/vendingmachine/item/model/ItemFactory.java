package com.bogdan.vendingmachine.item.model;

import com.bogdan.vendingmachine.item.products.Beverage;
import com.bogdan.vendingmachine.item.products.Candy;
import com.bogdan.vendingmachine.item.products.Chips;
import com.bogdan.vendingmachine.item.products.Chocolate;

public class ItemFactory {

	public static Item getItem(String input, String name, String type, Integer id, Double price, Integer qty) {
		if (input.equalsIgnoreCase("Beverage")) {
			return new Beverage(name, type, id, price, qty);
		} else if (input.equalsIgnoreCase("Chocolate")) {
			return new Chocolate(name, type, id, price, qty);
		} else if (input.equalsIgnoreCase("Candy")) {
			return new Candy(name, type, id, price, qty);
		} else if (input.equalsIgnoreCase("Chips")) {
			return new Chips(name, type, id, price, qty);
		}
		return null;
	}
}
