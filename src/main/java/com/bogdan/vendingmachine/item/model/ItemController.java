package com.bogdan.vendingmachine.item.model;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ItemController {
	public static List<Item> itemList = new ArrayList<>();

	public static void main(String[] args) throws FileNotFoundException {
		List<Item> itemList = new ArrayList<>();
		ItemFactory itemFactory = new ItemFactory();

		File itemsFile = new File(
				"/Users/bogdanturcu/eclipse-workspace/com.bogdan.vendingmachine/src/main/java/com/bogdan/vendingmachine/item/products/Items.txt");

		Scanner itemsFileScanner = new Scanner(itemsFile);

		/**
		 * reads items.txt, creates item, and stores it in a list
		 */
		while (itemsFileScanner.hasNextLine()) {
			String i = itemsFileScanner.nextLine();
			String[] itemString = i.split("\\s");

			Item item = itemFactory.getItem(itemString[0], itemString[1], itemString[2],
					Integer.parseInt(itemString[3]), Double.parseDouble(itemString[4]),
					Integer.parseInt(itemString[5]));

			itemList.add(item);
		}
		itemsFileScanner.close();

		for (Item item : itemList) {
			System.out.println(item);
		}
	}

}
