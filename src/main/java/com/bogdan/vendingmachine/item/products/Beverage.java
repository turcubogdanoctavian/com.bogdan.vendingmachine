package com.bogdan.vendingmachine.item.products;

import com.bogdan.vendingmachine.item.model.Item;

public class Beverage extends Item {

	private String name;
	private String type;
	private Double price;
	private Integer id;
	private Integer qty;

	public Beverage(String name, String type, Integer id, Double price, Integer qty) {
		this.name = name;
		this.type = type;
		this.id = id;
		this.price = price;
		this.qty = qty;
	}

	@Override
	public void setName(String name) {
		this.name = name;

	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String getType() {
		return this.type;
	}

	@Override
	public void setPrice(Double price) {
		this.price = price;

	}

	@Override
	public double getPrice() {
		return this.price;
	}

	@Override
	public void setQuantity(Integer qty) {
		this.qty = qty;

	}

	@Override
	public Integer getQuanity() {
		return this.qty;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((price == null) ? 0 : price.hashCode());
		result = prime * result + ((qty == null) ? 0 : qty.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Beverage other = (Beverage) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (price == null) {
			if (other.price != null)
				return false;
		} else if (!price.equals(other.price))
			return false;
		if (qty == null) {
			if (other.qty != null)
				return false;
		} else if (!qty.equals(other.qty))
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		return true;
	}

	@Override
	public void setId(Integer id) {
		this.id = id;

	}

	@Override
	public Integer getId() {
		return this.id;
	}

}
