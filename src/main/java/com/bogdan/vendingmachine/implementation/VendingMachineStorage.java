package com.bogdan.vendingmachine.implementation;

import java.io.File;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import com.bogdan.vendingmachine.exception.SoldOutException;
import com.bogdan.vendingmachine.item.model.Item;
import com.bogdan.vendingmachine.item.model.ItemFactory;

public class VendingMachineStorage {
	static List<Item> itemList = new ArrayList<>();
	static Map<Integer, Item> itemMap = new HashMap<>();

	public static void printItems() throws FileNotFoundException {

		/*
		 * Scans items
		 */
		scanItems();

		for (Item item : itemList) {
			System.out.println("------------------------------------------------------");
			System.out.println(item);
		}
		System.out.println("------------------------------------------------------");

	}

	public static Map<Integer, Item> mapItems(List<Item> itemList) throws FileNotFoundException {
		/*
		 * Scans items
		 */
		scanItems();

		/**
		 * 
		 * 
		 * 
		 * Maps items by id
		 * 
		 * 
		 * 
		 */
		for (Item item : itemList) {
			itemMap.put(item.getId(), item);
		}

		return itemMap;
	}

	public static String getStringItem(Integer id) throws FileNotFoundException {
		/*
		 * Scans items
		 */
		scanItems();
		mapItems(scanItems());

		/**
		 * lowers items quantity and returns item
		 */

		Item item = itemMap.get(id);
		int qty = item.getQuanity();
		if (qty == 0) {
			System.out.println("There are no " + item.getName() + " left. Please select another item");
		} else {
			item.setQuantity(qty - 1);
			return "Ejecting " + item.getName() + "-" + item.getType() + ", priced " + item.getPrice() + "\n"
					+ "Please retrieve your item!" + "\n" + "-----------------------------------";
		}
		return null;
	}

	public static Item getItem(Integer id) throws FileNotFoundException, SoldOutException {
		/**
		 * lowers items quantity and returns item
		 */
		scanItems(); // scans items
		mapItems(scanItems());

		Item item = itemMap.get(id);
		int qty = item.getQuanity();
		if (qty == 0) {
			System.out.println("There are no " + item.getName() + " left.");
			throw new SoldOutException();
		} else {
			item.setQuantity(qty - 1);
			return item;
		}
	}

	public static List<Item> scanItems() throws FileNotFoundException {
		ItemFactory itemFactory = new ItemFactory();

		File itemsFile = new File(
				"/Users/bogdanturcu/eclipse-workspace/com.bogdan.vendingmachine/src/main/java/com/bogdan/vendingmachine/item/products/Items.txt");

		Scanner itemsFileScanner = new Scanner(itemsFile);

		/**
		 * 
		 * 
		 * 
		 * reads items.txt, creates item, and stores it in a list
		 * 
		 * 
		 * 
		 */
		while (itemsFileScanner.hasNextLine()) {
			String i = itemsFileScanner.nextLine();
			String[] itemString = i.split("\\s");

			Item item = ItemFactory.getItem(itemString[0], itemString[1], itemString[2],
					Integer.parseInt(itemString[3]), Double.parseDouble(itemString[4]),
					Integer.parseInt(itemString[5]));

			itemList.add(item);

		}
		itemsFileScanner.close();

		return itemList;
	}

}
