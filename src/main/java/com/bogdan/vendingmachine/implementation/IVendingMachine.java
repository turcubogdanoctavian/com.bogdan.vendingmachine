package com.bogdan.vendingmachine.implementation;

import java.io.FileNotFoundException;
import java.io.IOException;

import com.bogdan.vendingmachine.exception.NotFullPaidException;
import com.bogdan.vendingmachine.exception.NotSufficientChangeException;
import com.bogdan.vendingmachine.exception.SoldOutException;

public interface IVendingMachine {

	public Double insertCoins() throws FileNotFoundException, SoldOutException;

	public Boolean selectProduct(Double coinInput) throws FileNotFoundException, SoldOutException, NotFullPaidException,
			NotSufficientChangeException, IOException;

	public Boolean confirmOrder()
			throws FileNotFoundException, NotSufficientChangeException, IOException, SoldOutException;

	public void returnChange() throws FileNotFoundException, SoldOutException;

}
