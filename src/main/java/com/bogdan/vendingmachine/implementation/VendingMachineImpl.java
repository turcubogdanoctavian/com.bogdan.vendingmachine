package com.bogdan.vendingmachine.implementation;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;

import com.bogdan.vendingmachine.coin.CoinEnum;
import com.bogdan.vendingmachine.coin.model.CoinStorage;
import com.bogdan.vendingmachine.exception.NotFullPaidException;
import com.bogdan.vendingmachine.exception.NotSufficientChangeException;
import com.bogdan.vendingmachine.exception.SoldOutException;
import com.bogdan.vendingmachine.item.model.Item;

import junit.framework.Assert;

public class VendingMachineImpl implements IVendingMachine {
	static Double coinInput;
	Double change;
	String order;
	Integer selectedProduct;
	Item item;
	Double sum = 0.0;
	public static List<Double> listOfCoinInput = new ArrayList<>();

	Scanner sc = new Scanner(System.in);

	public Double insertCoins() throws FileNotFoundException, SoldOutException {
		/**
		 * asks user the coin input
		 */
		String input = null;

		System.out.println("Please pay in coins (0.1, 0.5, 1.0, 2.5)");
		input = sc.next();
		sum = sum + Double.parseDouble(input);

		listOfCoinInput.add(Double.parseDouble(input));

		while (!(input.equals("confirm"))) {
			System.out.println("Imput more coins / Confirm");
			input = sc.next();

			if (!(input.equals("confirm"))) {
				listOfCoinInput.add(Double.parseDouble(input));

				sum += Double.parseDouble(input);
			}
			System.out.println("-----------------------------------");
		}
		System.out.println("Your coin input is: " + sum);

		coinInput = sum;
		return coinInput;

	}

	public Boolean selectProduct(Double coinInput) throws SoldOutException, NotSufficientChangeException, IOException {
		/*
		 * Scans items
		 */
		VendingMachineStorage.scanItems();
		VendingMachineStorage.mapItems(VendingMachineStorage.scanItems());

		Boolean selectSuccesfull = false;
		/**
		 * selects product and checks if your coin input eligible
		 */
		System.out.println("Please select an item by id:");
		selectedProduct = sc.nextInt();
		System.out.println("-----------------------------------");

		/**
		 * checks if the item is sold out
		 */

		item = VendingMachineStorage.getItem(selectedProduct);
		if (coinInput < item.getPrice()) {
			System.out.println("You need more money for that item");
			try {
				throw new NotFullPaidException();
			} catch (NotFullPaidException e) {
				/**
				 * returns money in case of not fully paid
				 */
				selectSuccesfull = true;
				System.out.println("-----------------------------------");
				System.out.println(
						"Your order is cancelled // Returning your coins.." + returnInputInCaseOfCancelledOrder());

			}
		} else if (coinInput == item.getPrice()) {
			CoinStorage.addToCoinBank(listOfCoinInput);
			selectSuccesfull = true;
			confirmOrder();
		} else if (coinInput > item.getPrice()) {
			change = coinInput - item.getPrice();
			/*
			 * add input to coin bank
			 */
			CoinStorage.addToCoinBank(listOfCoinInput);
			selectSuccesfull = true;
			confirmOrder();

		}

		return selectSuccesfull;
	}

	public void buyItemSteps() {
		/**
		 * steps of buying an item
		 */
		System.out.println("Steps:" + "\n" + "---------------------" + " \n" + "1.Insert Coins" + "\n"
				+ "2.Select Product" + "\n" + "---------------------" + " \n"
				+ "You can only input coins one at a time." + "\n" + "---------------------");
	}

	public Boolean confirmOrder() throws NotSufficientChangeException, IOException, SoldOutException {
		/**
		 * gives you the opportunity of canceling the order
		 */
		Boolean orderConfirmed = false;

		VendingMachineStorage.scanItems(); // scans items
		VendingMachineStorage.mapItems(VendingMachineStorage.scanItems()); // maps items

		System.out.println("Your item is:" + item.getName() + " " + item.getType() + ", priced " + item.getPrice());
		System.out.println("Cancel / Confirm order?");
		order = sc.next().toLowerCase();
		System.out.println("-----------------------------------");
		if (order.equals("cancel")) {
			System.out.println("Returning input " + returnInputInCaseOfCancelledOrder());
			CoinStorage.decreaseFromCoinBank(coinInput);
		} else if (order.equals("confirm")) {
			/*
			 * if order is confirmed, item and change is returned
			 */
			try {
				/*
				 * decreases change from coin bank
				 */
				CoinStorage.decreaseFromCoinBank(change);
			} catch (IOException e) {

			} catch (NotSufficientChangeException e) {
				System.out.println("Not sufficient change in the Coin Bank // Order is cancelled");
				System.out.println(returnInputInCaseOfCancelledOrder());
			}
			orderConfirmed = true;
			System.out.println(VendingMachineStorage.getStringItem(selectedProduct));
			returnChange();
		}
		return orderConfirmed;
	}

	public static Double returnInputInCaseOfCancelledOrder() {
		return coinInput;
	}

	public void returnChange() {
		/**
		 * returns change
		 */

		double change = coinInput - item.getPrice();
		System.out.println("Returning change.. Please wait, your change is:" + change);
		int numPenny;
		int numNickel;
		int numDime;
		int numQuarter;
		numQuarter = CoinEnum.QUARTER.toDenomination(change);

		if (numQuarter != 0) {
			System.out.println("Quarters: " + numQuarter);
			change = change - 2.5;
		} else {
			System.out.println("Quarters: " + numQuarter);
		}
		numDime = CoinEnum.DIME.toDenomination(change);

		if (numDime != 0) {
			System.out.println("Dimes: " + numDime);
			change = change - 1.0;
		} else {
			System.out.println("Dimes: " + numDime);
		}

		numNickel = CoinEnum.NICKEL.toDenomination(change);
		if (numNickel != 0) {
			System.out.println("Nickels: " + numNickel);
			change = change - 0.5;
		} else {
			System.out.println("Nickels: " + numNickel);
		}

		numPenny = CoinEnum.PENNY.toDenomination(change);
		System.out.println("Pennies: " + numPenny);

		coinInput = null;
	}
}
