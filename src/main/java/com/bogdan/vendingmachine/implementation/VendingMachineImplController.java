package com.bogdan.vendingmachine.implementation;

import java.io.IOException;
import java.util.Scanner;

import com.bogdan.vendingmachine.implementation.VendingMachineStorage;
import com.bogdan.vendingmachine.coin.model.CoinStorage;
import com.bogdan.vendingmachine.exception.NotFullPaidException;
import com.bogdan.vendingmachine.exception.NotSufficientChangeException;
import com.bogdan.vendingmachine.exception.SoldOutException;
import com.bogdan.vendingmachine.implementation.VendingMachineImpl;

public class VendingMachineImplController extends VendingMachineImpl {
	public static void main(String[] args)
			throws SoldOutException, NotFullPaidException, IOException, NotSufficientChangeException {
		Scanner sc = new Scanner(System.in);
		VendingMachineImpl vendingMachineImpl = new VendingMachineImpl();

		// VendingMachineStorage.scanItems();
		CoinStorage.scanCoins();
		VendingMachineStorage.printItems();
		// VendingMachineStorage.mapItems();
		vendingMachineImpl.buyItemSteps();
		/**
		 * Throws SoldOutException if there is no item left, change is returned
		 */
		coinInput = vendingMachineImpl.insertCoins();
		try {
			vendingMachineImpl.selectProduct(coinInput);
		} catch (SoldOutException e) {
			VendingMachineImpl.returnInputInCaseOfCancelledOrder();
		}

		sc.close();
	}
}
