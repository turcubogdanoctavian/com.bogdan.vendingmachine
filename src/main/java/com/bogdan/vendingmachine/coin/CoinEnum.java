package com.bogdan.vendingmachine.coin;

public enum CoinEnum {
	PENNY(0.1), NICKEL(0.5), DIME(1.0), QUARTER(2.5);

	private double value;

	CoinEnum(double value) {
		this.value = value;
	}

	double getValue() {
		return this.value;
	}

	public int toDenomination(Double coinInput) {
		return (int) (coinInput / this.value);
	}
}
