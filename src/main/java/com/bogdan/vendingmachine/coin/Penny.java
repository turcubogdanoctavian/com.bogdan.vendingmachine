package com.bogdan.vendingmachine.coin;

import com.bogdan.vendingmachine.coin.model.Coin;

public class Penny extends Coin {
	private Integer qty;
	private String value;

	public Penny(String value, Integer qty) {
		this.value = value;
		this.qty = qty;
	}

	@Override
	public void setQuantity(Integer qty) {
		this.qty = qty;
	}

	@Override
	public Integer getQuantity() {
		return qty;
	}

	@Override
	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public String getValue() {

		return value;
	}
}
