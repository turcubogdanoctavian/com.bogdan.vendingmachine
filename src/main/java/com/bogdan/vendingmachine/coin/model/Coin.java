package com.bogdan.vendingmachine.coin.model;

public abstract class Coin {

	public abstract void setValue(String value);

	public abstract String getValue();

	public abstract void setQuantity(Integer qty);

	public abstract Integer getQuantity();
}
