package com.bogdan.vendingmachine.coin.model;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import com.bogdan.vendingmachine.coin.CoinEnum;
import com.bogdan.vendingmachine.exception.NotSufficientChangeException;

public class CoinStorage {
	public static List<Coin> coinList = new ArrayList<>();
	public static Map<String, Integer> coinMap = new HashMap<>();

	static String coinName = null;

	public static List<Coin> scanCoins() throws FileNotFoundException {
		CoinFactory coinFactory = new CoinFactory();
		File startBankFile = new File(
				"/Users/bogdanturcu/eclipse-workspace/com.bogdan.vendingmachine/src/main/java/com/bogdan/vendingmachine/coin/startBank.txt");

		Scanner startBankFileScanner = new Scanner(startBankFile);

		/**
		 * Reads coin.txt and creates a list with the amount of each coin stored in the
		 * vending machine
		 */
		while (startBankFileScanner.hasNextLine()) {
			String i = startBankFileScanner.nextLine();
			String[] coinString = i.split("\\s");

			Coin coin = coinFactory.getCoin(coinString[0], Integer.parseInt(coinString[1]));

			coinList.add(coin);
		}
		startBankFileScanner.close();

		for (Coin coin : coinList) {
			coinMap.put(coin.getValue(), coin.getQuantity());
		}
		return coinList;
	}

	public static Integer getCoinQuanity(String coinName) {
		Integer coinQuantity = coinMap.get(coinName);
		System.out.println(coinQuantity);
		return coinQuantity;
	}

	public static void addToCoinBank(List<Double> listOfCoinInput) {
		/**
		 * adds +1 to certain type of coin quantity in the bank
		 */
		for (Double input : listOfCoinInput) {

			Integer coinQty = 0;
			if (input == 0.1) {
				coinQty = coinMap.get("Penny");
				coinMap.put("Penny", coinQty + 1);

			}
			if (input == 0.5) {
				coinQty = coinMap.get("Nickel");
				coinMap.put("Nickel", coinQty + 1);

			}
			if (input == 1.0) {
				coinQty = coinMap.get("Dime");
				coinMap.put("Dime", coinQty + 1);
			}
			if (input == 2.5) {
				coinQty = coinMap.get("Quarter");
				coinMap.put("Quarter", coinQty + 1);
			}
		}
	}

	public static void decreaseFromCoinBank(Double change) throws NotSufficientChangeException, IOException {
		/**
		 * decreases -1 to the certain type of coin quantity in the bank
		 */
		File updatedBankFile = new File(
				"/Users/bogdanturcu/eclipse-workspace/com.bogdan.vendingmachine/src/main/java/com/bogdan/vendingmachine/coin/updatedBank.txt");
		BufferedWriter updatedBankWriter = new BufferedWriter(new FileWriter(updatedBankFile));

		int numPenny;
		int numNickel;
		int numDime;
		int numQuarter;
		int coinQty = 0;

		/**
		 * checks each type of coin in the bank if it can be decreased(if it is > 0), if
		 * it cannot be decreased, exception is thrown
		 */
		numQuarter = CoinEnum.QUARTER.toDenomination(change);
		if (numQuarter != 0) {
			change = change - numQuarter * 2.5;
			coinQty = coinMap.get("Quarter");
			if (coinQty != 0) {
				coinMap.put("Quarter", coinQty - numQuarter);
				updatedBankWriter.write("Quarter: " + coinMap.get("Quarter") + "\n");
			} else {
				updatedBankWriter.close();
				throw new NotSufficientChangeException();
			}
		} else {
			updatedBankWriter.write("Quarter: " + coinMap.get("Quarter") + "\n");
		}

		numDime = CoinEnum.DIME.toDenomination(change);
		if (numDime != 0) {
			change = change - numDime * 1.0;
			coinQty = coinMap.get("Dime");
			if (coinQty != 0) {
				coinMap.put("Dime", coinQty - numDime);
				updatedBankWriter.write("Dime: " + coinMap.get("Dime") + "\n");
			} else {
				updatedBankWriter.close();
				throw new NotSufficientChangeException();
			}

		} else {
			updatedBankWriter.write("Dime: " + coinMap.get("Dime") + "\n");
		}

		numNickel = CoinEnum.NICKEL.toDenomination(change);
		if (numNickel != 0) {
			change = change - numNickel * 0.5;
			coinQty = coinMap.get("Nickel");
			if (coinQty != 0) {
				coinMap.put("Nickel", coinQty - numNickel);
				updatedBankWriter.write("Nickel: " + coinMap.get("Nickel") + "\n");
			} else {
				updatedBankWriter.close();
				throw new NotSufficientChangeException();
			}
		} else {
			updatedBankWriter.write("Nickel: " + coinMap.get("Nickel") + "\n");
		}

		numPenny = CoinEnum.PENNY.toDenomination(change);

		if (numPenny != 0) {
			change = change - numPenny * 0.1;
			coinQty = coinMap.get("Penny");
			if (coinQty != 0) {
				coinMap.put("Penny", coinQty - numPenny);
				updatedBankWriter.write("Penny: " + coinMap.get("Penny"));
			} else {
				updatedBankWriter.close();
				throw new NotSufficientChangeException();
			}
		} else {
			updatedBankWriter.write("Penny: " + coinMap.get("Penny"));
		}
		updatedBankWriter.close();
	}

}
