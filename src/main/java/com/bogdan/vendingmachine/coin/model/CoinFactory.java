package com.bogdan.vendingmachine.coin.model;

import com.bogdan.vendingmachine.coin.Dime;
import com.bogdan.vendingmachine.coin.Nickel;
import com.bogdan.vendingmachine.coin.Penny;
import com.bogdan.vendingmachine.coin.Quarter;

public class CoinFactory {
	public Coin getCoin(String input, Integer quantity) {
		if (input.equalsIgnoreCase("Quarter")) {
			return new Quarter(input, quantity);
		} else if (input.equalsIgnoreCase("Dime")) {
			return new Dime(input, quantity);
		} else if (input.equalsIgnoreCase("Nickel")) {
			return new Nickel(input, quantity);
		} else if (input.equalsIgnoreCase("Penny")) {
			return new Penny(input, quantity);
		}
		return null;
	}
}
