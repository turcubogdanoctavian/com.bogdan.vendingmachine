package com.bogdan.vendingmachine.implementation;

import static org.junit.Assert.assertEquals;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.junit.Test;

import com.bogdan.vendingmachine.exception.NotSufficientChangeException;
import com.bogdan.vendingmachine.exception.SoldOutException;
import com.bogdan.vendingmachine.implementation.VendingMachineStorage;

public class VendingMachineImplTest {

	VendingMachineImpl test = new VendingMachineImpl();
	Double coinInput;

	@Test
	public void testInsertCoins() throws FileNotFoundException, SoldOutException {

		coinInput = test.insertCoins();
		assertEquals(true, coinInput); // insert must be succesfull
	}

	@Test
	public void testSelectProduct() throws SoldOutException, NotSufficientChangeException, IOException {
		VendingMachineStorage.scanItems();

		Boolean result = test.selectProduct(coinInput);
		assertEquals(2.5, result); // select must be succesfull
	}

	@Test
	public void testConfirmOrder() throws NotSufficientChangeException, IOException, SoldOutException {
		VendingMachineStorage.scanItems();

		Boolean result = test.confirmOrder();
		assertEquals(true, result);// confirm must be succesfull
	}

}
