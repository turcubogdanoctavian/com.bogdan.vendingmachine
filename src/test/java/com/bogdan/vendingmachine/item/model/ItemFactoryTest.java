package com.bogdan.vendingmachine.item.model;

import static org.junit.Assert.*;

import org.junit.Test;

public class ItemFactoryTest {

	@Test
	public void testGetItem() {

		Item cola = ItemFactory.getItem("Beverage", "Cola", "Lime", 1, 3.5, 2);
		assertTrue(cola instanceof Item);
	}

}
